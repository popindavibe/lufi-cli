# Contribute

## Report issues, ask some questions, ask for features…

For all this, please post an issue at <https://framagit.org/luc/lufi-cli/issues>. If you don't have a `framagit.org` account, you can register for free, and even use your github account to do it.

All issues opened on github will be asked to move here.

## Make a donation

You can make a donation to the author on [Tipeee](https://www.tipeee.com/fiat-tux) or on [Liberapay](https://liberapay.com/sky/).
