#!/usr/bin/env node
/* vim:set sts=4 sw=4 ts=4 ft=javascript expandtab:*/
'use strict';

const program = require('commander'),
         sjcl = require('sjcl'),
           fs = require('fs');

var WebSocket = require('faye-websocket'),
        Gauge = require('gauge'),
   fileExists = require('file-exists'),
   enfsensure = require("enfsensure"),
      request = require('request'),
     filesize = require('file-size'),
       moment = require('moment');


var completed = false,
            a = new Array(),
         home = getUserHome(),
    bar, key, ws;

function list(val) {
    return val.split(',');
}

function getUserHome() {
    return process.env.HOME || process.env.USERPROFILE;
}

enfsensure.ensureDirSync(home+'/.local/share/lufi', { mode: 750 });

program
    .version('0.0.1')
    .option('-u, --upload <files>', 'a comma-separated list of files to upload', list)
    .option('-s, --server <url>', 'the server where you want to upload your files to')
    .option('-e, --expiration <integer>', 'in how many days your file will expire (may be sooner due to server limits)', parseInt)
    .option('-b, --burn-after-reading', 'your file will be deleted right after the first download')
    .option('-d, --download <urls>', 'a comma-separated list of URLs to download', list)
    .option('-i, --infos', 'get informations about your uploaded files')
    .option('-v, --verbose', 'verbose output')
    .parse(process.argv);

if (program.infos) {
    if (fileExists(home+'/.local/share/lufi/files.json')) {
        var files = JSON.parse(fs.readFileSync(home+'/.local/share/lufi/files.json'));
        files.forEach(function(el, index) {
            getFileInfos(el);
        });
    } else {
        console.log('You have not uploaded files yet. (no %s/.local/share/lufi/files.json file)', home);
        if (!program.download && !program.upload) {
            process.exit(0);
        }
    }
}

if (program.download) {
    program.download.forEach(function(el, index) {
        var [url, k] = el.split('#');
                 key = k;

        bar = new Gauge(process.stderr, {
            theme: 'colorASCII'
        });
        console.log('About to download %s', url);
        bar.show(url+': 0%', 0);
        spawnWebsocket(url.replace(/http/, 'ws').replace(/\/r\//, '/download/'), 0, (index === program.download.length - 1 && !program.upload), url);
    });
}

function spawnWebsocket(url, pa, exit, real_url) {
    ws = new WebSocket.Client(url);

    ws.on('open', function() {
        if (program.verbose) {
            console.log('Connection is established!');
        }
        ws.send('{"part":'+pa+'}');
    });
    ws.on('close', function() {
        if (program.verbose) {
            console.log('Connection is closed');
        }
        if (!completed) {
            if (program.verbose) {
                console.log('Connection closed. Retrying to get slice '+pa);
            }
            ws = spawnWebsocket(url, pa);
        } else {
            if (exit) {
                process.exit(0);
            }
        }
    });
    ws.on('message', function(e) {
        var res  = e.data.split('XXMOJOXX');
        var json = res.shift();
        var data = JSON.parse(json);

        if (data.msg !== undefined) {
            // Something went wrong
            console.log('[ERROR] %s', data.msg);
            process.exit(1);
        } else {
            // update the progress bar
            var percent = Math.round(100 * (data.part + 1)/data.total);
            bar.show(real_url+': '+percent+'%', percent / 100);

            if (program.verbose) {
                console.log('Getting slice '+(data.part + 1)+' of '+data.total);
            }
            // get the data slice
            var slice   = JSON.parse(res.shift());
            try {
                // decrypt the data slice
                var b64 = sjcl.decrypt(key, slice);
                // put it in the result array
                a[data.part] = b64;

                if (data.part + 1 === data.total) {
                    bar.hide();
                    // we're good: download is over
                    var blob = a.join('');

                    var name = getName(data.name);
                    fs.writeFile(name, blob, 'base64', function(err) {
                        if (err !== null) {
                            console.log('Error while writing the file to the filesystem: %s', err);
                        } else {
                            console.log('%s has been successfully downloaded', name);
                        }
                    });

                    ws.send('{"ended":true}');
                    completed = true;
                    ws.close();
                } else {
                    if (ws.readyState === 3) {
                        ws = spawnWebsocket(url, data.part + 1);
                    } else {
                        ws.on('close', function() {
                            if (program.verbose) {
                                console.log('Connection is closed');
                            }
                            if (!completed) {
                                if (program.verbose) {
                                    console.log('Connection closed. Retrying to get slice '+(data.part + 1));
                                }
                                ws = spawnWebsocket(url, data.part + 1);
                            }
                        });
                        ws.on('error', function() {
                            if (program.verbose) {
                                console.log('Error. Retrying to get slice '+(data.part + 1));
                            }
                            ws = spawnWebsocket(url, data.part + 1);
                        });
                        ws.send('{"part":'+(url, data.part + 1)+'}');
                    }
                }
            } catch(err) {
                if (err.message === 'ccm: tag doesn\'t match') {
                    console.log('It seems that the key in your URL is incorrect. Please, verify your URL.');
                    process.exit(2);
                } else {
                    console.log(err.message);
                    process.exit(3);
                }
            }
        }
    });
    ws.on('error', function() {
        if (program.verbose) {
            console.log('Error. Retrying to get slice '+pa);
        }
        ws = spawnWebsocket(url, pa);
    });
    //return ws;
}

function getName(name, n) {
    if (n === undefined || n === null) {
        n = 0;
    }
    var m = n + 1;
    if (fileExists(name)) {
        var tmp = name.match(new RegExp('(\.[^\.]+)$'));
        if (tmp !== null) {
            if (name.match('('+n+')')) {
                name = name.replace('('+n+')', '('+m+')');
            } else {
                name = name.replace(new RegExp(tmp[1]+'$'), '('+m+')'+tmp[1]);
            }
        } else {
            name = name.replace(new RegExp('\('+n+'\)?$'), '('+m+')');
        }
        return getName(name, m);
    } else {
        return name;
    }
}

function getFileInfos(file) {
    request.post({
        url: file.url.replace(/\/r\/.*/, '/c'),
        form: {
            'short': file.short,
            'token': file.token
        },
    }, function (err, response, body) {
        console.log('%s:', file.name);
        console.log('  url: %s', file.url);
        console.log('  size: %s', filesize(file.size).human());
        console.log('  created at: %s', moment.unix(file.created_at).format('dddd, MMMM Do YYYY'));
        console.log('  delay: %d', file.delay);
        console.log('    will be deleted on: %s', moment.unix(file.created_at + 86400 * file.delay).format('dddd, MMMM Do YYYY'));
        console.log('  burn after reading: %s', file.del_at_first_view);
        if (!err && response.statusCode == 200) {
            var data = JSON.parse(body);
            console.log('  deleted: %s', data.deleted);
            console.log('  counter: %d', data.counter);
        }
        console.log('----');
    });
}
